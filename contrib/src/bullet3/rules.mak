BULLET_NAME := bullet3
BULLET_VERSION := 3.06
BULLET_URL := https://github.com/bulletphysics/$(BULLET_NAME)/archive/refs/tags/$(BULLET_VERSION).zip

PKGS += $(BULLET_NAME)
ifeq ($(call need_pkg,'$(BULLET_NAME)'),)
PKGS_FOUND += $(BULLET_NAME)
endif

BULLET_CMAKECONF := -DJSONCPP_WITH_TESTS=OFF

$(TARBALLS)/$(BULLET_NAME)-$(BULLET_VERSION).zip:
	$(call download,$(BULLET_URL))

.sum-$(BULLET_NAME): $(BULLET_NAME)-$(BULLET_VERSION).zip

$(BULLET_NAME): $(BULLET_NAME)-$(BULLET_VERSION).zip
	$(UNPACK)
	(cd $(BULLET_NAME)-$(BULLET_VERSION) && patch -flp1) < $(SRC)/bullet3/bullet3.patch
	cd ..
	mv $(BULLET_NAME)-$(BULLET_VERSION) $(BULLET_NAME)

.$(BULLET_NAME): $(BULLET_NAME) toolchain.cmake .sum-$(BULLET_NAME)
	mkdir -p $</build
	cd $</build && $(CMAKE) $(CMAKECONF) $(BULLET_CMAKECONF) ..
	cd $</build && $(MAKE) install
	touch $@