MICROHTTPD_NAME=libmicrohttpd
MICROHTTPD_VERSION := 1.0.1
MICROHTTPD_URL := https://ftp.gnu.org/gnu/$(MICROHTTPD_NAME)/$(MICROHTTPD_NAME)-$(MICROHTTPD_VERSION).tar.gz

PKGS += $(MICROHTTPD_NAME)
# Check if openssl or libmicrohttpd is already present on the system
ifeq ($(call need_pkg,'$(MICROHTTPD_NAME)'),)
PKGS_FOUND += $(MICROHTTPD_NAME)
endif

DEPS_libmicrohttpd += gnutls

$(TARBALLS)/$(MICROHTTPD_NAME)-$(MICROHTTPD_VERSION).tar.gz:
	$(call download,$(MICROHTTPD_URL))

libmicrohttpd: $(MICROHTTPD_NAME)-$(MICROHTTPD_VERSION).tar.gz
	$(UNPACK)
	$(MOVE)

.libmicrohttpd: libmicrohttpd .sum-libmicrohttpd
	cd $< && $(HOSTVARS) ./configure --disable-documentation $(HOSTCONF)
	cd $< && $(MAKE)
	cd $< && $(MAKE) install
	touch $@

.sum-libmicrohttpd: $(MICROHTTPD_NAME)-$(MICROHTTPD_VERSION).tar.gz
