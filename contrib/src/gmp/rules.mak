# GNU Multiple Precision Arithmetic


ifdef HAVE_MACOSX
ifeq ($(ARCH),arm64)
GMP_VERSION := 6.2.99-20221117121717
GMP_URL := https://gmplib.org/download/snapshot/gmp-next/gmp-$(GMP_VERSION).tar.zst
else
GMP_VERSION := 6.2.1
GMP_URL := $(GNU)/gmp/gmp-$(GMP_VERSION).tar.bz2
endif
else
GMP_VERSION := 6.2.1
GMP_URL := $(GNU)/gmp/gmp-$(GMP_VERSION).tar.bz2
endif

$(TARBALLS)/gmp-$(GMP_VERSION).tar.bz2:
	$(call download,$(GMP_URL))

.sum-gmp: gmp-$(GMP_VERSION).tar.bz2

gmp: gmp-$(GMP_VERSION).tar.bz2 .sum-gmp
	$(UNPACK)
ifdef HAVE_IOS
	$(APPLY) $(SRC)/gmp/clock_gettime.patch
endif
ifdef HAVE_MACOSX
	$(APPLY) $(SRC)/gmp/clock_gettime.patch
endif
	$(MOVE)

.gmp: gmp
ifdef HAVE_IOS
	$(RECONF)
	cd $< && $(HOSTVARS) CFLAGS="$(CFLAGS) -O3" ./configure --disable-assembly --without-clock-gettime $(HOSTCONF)
else
ifdef HAVE_MACOSX
	$(RECONF)
	cd $< && $(HOSTVARS) ./configure --without-clock-gettime $(HOSTCONF)
else
	cd $< && $(HOSTVARS) ./configure $(HOSTCONF)
endif
endif
	cd $< && $(MAKE) install
	touch $@
