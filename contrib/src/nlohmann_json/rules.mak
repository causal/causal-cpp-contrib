JSON_NAME := nlohmann_json
JSON_VERSION := 3.10.3
JSON_URL := https://github.com/nlohmann/json/archive/refs/tags/v$(JSON_VERSION).tar.gz

PKGS += $(JSON_NAME)
ifeq ($(call need_pkg,'$(JSON_NAME)'),)
PKGS_FOUND += $(JSON_NAME)
endif

JSON_CMAKECONF := -DJSONCPP_WITH_TESTS=OFF

$(TARBALLS)/$(JSON_NAME)-v$(JSON_VERSION).tar.gz:
	$(call download,$(JSON_URL))

.sum-$(JSON_NAME): $(JSON_NAME)-v$(JSON_VERSION).tar.gz

$(JSON_NAME): $(JSON_NAME)-v$(JSON_VERSION).tar.gz
	$(UNPACK)
	mv json-$(JSON_VERSION) $(JSON_NAME)

.$(JSON_NAME): $(JSON_NAME) toolchain.cmake .sum-$(JSON_NAME)
	mkdir -p $</build
	cd $</build && $(CMAKE) $(CMAKECONF) $(JSON_CMAKECONF) ..
	cd $</build && $(MAKE) install
	touch $@