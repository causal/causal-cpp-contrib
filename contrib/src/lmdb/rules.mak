LMDB_NAME := lmdb
LMDB_VERSION := 0.9.29
LMDB_URL := https://github.com/$(LMDB_NAME)/lmdb/archive/refs/tags/LMDB_$(LMDB_VERSION).tar.gz

PKGS += $(LMDB_NAME)
ifeq ($(call need_pkg,'$(LMDB_NAME)'),)
PKGS_FOUND += $(LMDB_NAME)
endif

$(TARBALLS)/$(LMDB_NAME)_$(LMDB_VERSION).tar.gz:
	$(call download,$(LMDB_URL))

.sum-$(LMDB_NAME): $(LMDB_NAME)_$(LMDB_VERSION).tar.gz

ifdef HAVE_ANDROID
$(LMDB_NAME): $(LMDB_NAME)_$(LMDB_VERSION).tar.gz
	$(UNPACK)
	mv $(LMDB_NAME)-LMDB_$(LMDB_VERSION) $(UNPACK_DIR)
	$(APPLY) $(SRC)/lmdb/lmdb_android.patch
	mv $(UNPACK_DIR) $(LMDB_NAME)
else
$(LMDB_NAME): $(LMDB_NAME)_$(LMDB_VERSION).tar.gz
	$(UNPACK)
	mv $(LMDB_NAME)-LMDB_$(LMDB_VERSION) $(LMDB_NAME)
endif

ifdef HAVE_ANDROID
.$(LMDB_NAME): $(LMDB_NAME) toolchain.cmake .sum-$(LMDB_NAME)
	cd $</libraries/liblmdb && CFLAGS=-DMDB_USE_ROBUST=0 $(MAKE) prefix=/ && $(MAKE) DESTDIR="$(PREFIX)" prefix=/ install
	cp -f $(SRC)/lmdb/lmdb.pc ${PREFIX}/lib/pkgconfig
	touch $@
else
.$(LMDB_NAME): $(LMDB_NAME) toolchain.cmake .sum-$(LMDB_NAME)
	cd $</libraries/liblmdb && $(MAKE) prefix=/ && $(MAKE) DESTDIR="$(PREFIX)" prefix=/ install
	cp -f $(SRC)/lmdb/lmdb.pc ${PREFIX}/lib/pkgconfig
	touch $@
endif