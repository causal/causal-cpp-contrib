# JSONCPP
JSONCPP_VERSION := 1.9.3

JSONCPP_URL := https://github.com/open-source-parsers/jsoncpp/archive/$(JSONCPP_VERSION).tar.gz

PKGS += jsoncpp

ifeq ($(call need_pkg,"jsoncpp >= 1.9.3"),)
PKGS_FOUND += jsoncpp
endif

JSONCPP_CMAKECONF := -DJSONCPP_WITH_TESTS=OFF

$(TARBALLS)/jsoncpp-$(JSONCPP_VERSION).tar.gz:
	$(call download,$(JSONCPP_URL))

.sum-jsoncpp: jsoncpp-$(JSONCPP_VERSION).tar.gz

jsoncpp: jsoncpp-$(JSONCPP_VERSION).tar.gz .sum-jsoncpp
	$(UNPACK)
	$(MOVE)

.jsoncpp: jsoncpp toolchain.cmake
	mkdir -p $</build
	cd $</build && $(HOSTVARS) $(CMAKE) $(CMAKECONF) $(JSONCPP_CMAKECONF) ..
	cd $</build && $(MAKE) install
	touch $@
