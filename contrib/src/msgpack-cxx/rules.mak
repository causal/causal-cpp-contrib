# MSGPACK
MSGPACK_CXX_VERSION := cpp-4.1.3
MSGPACK_CXX_URL := https://github.com/msgpack/msgpack-c/archive/$(MSGPACK_CXX_VERSION).tar.gz

PKGS += msgpack-cxx
ifeq ($(call need_pkg,"msgpack-cxx >= 4.1.3"),)
PKGS_FOUND += msgpack-cxx
endif

DEPS_msgpack-cxx += msgpack

MSGPACK_CXX_CMAKECONF := -DMSGPACK_CXX17=ON \
		                 -DMSGPACK_BUILD_EXAMPLES=OFF

$(TARBALLS)/msgpack-c-$(MSGPACK_CXX_VERSION).tar.gz:
	$(call download,$(MSGPACK_CXX_URL))

.sum-msgpack-cxx: msgpack-c-$(MSGPACK_CXX_VERSION).tar.gz

msgpack-cxx: msgpack-c-$(MSGPACK_CXX_VERSION).tar.gz .sum-msgpack-cxx
	$(UNPACK)
	$(MOVE)

.msgpack-cxx: msgpack-cxx toolchain.cmake
	mkdir -p $</build
	cd $</build && $(HOSTVARS) $(CMAKE) $(CMAKECONF) $(MSGPACK_CXX_CMAKECONF) ..
	cd $</build && $(MAKE) install
	touch $@
