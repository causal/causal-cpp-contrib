GTEST_NAME := googletest
GTEST_VERSION := release-1.11.0
GTEST_URL := https://github.com/google/$(GTEST_NAME)/archive/refs/tags/$(GTEST_VERSION).tar.gz

PKGS += $(GTEST_NAME)
ifeq ($(call need_pkg,'$(GTEST_NAME)'),)
PKGS_FOUND += $(GTEST_NAME)
endif

GTEST_CMAKECONF := 

$(TARBALLS)/$(GTEST_NAME)-$(GTEST_VERSION).tar.gz:
	$(call download,$(GTEST_URL))

.sum-$(GTEST_NAME): $(GTEST_NAME)-$(GTEST_VERSION).tar.gz

$(GTEST_NAME): $(GTEST_NAME)-$(GTEST_VERSION).tar.gz
	$(UNPACK)
	mv $(UNPACK_DIR) $(GTEST_NAME)

.$(GTEST_NAME): $(GTEST_NAME) toolchain.cmake .sum-$(GTEST_NAME)
	mkdir -p $</build
	cd $</build && $(CMAKE) $(CMAKECONF) $(GTEST_CMAKECONF) ..
	cd $</build && $(MAKE) install
	touch $@