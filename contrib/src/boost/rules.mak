# BOOST

BOOST_VERSION := 1.79.0
BOOST_NAME := boost_1_79_0
LIBRARIES=atomic,chrono,container,context,contract,coroutine,date_time,exception,fiber,filesystem,graph,graph_parallel,headers,iostreams,locale,log,math,mpi,nowide,program_options,python,random,regex,serialization,stacktrace,system,test,thread,timer,type_erasure,wave
ifdef HAVE_ANDROID
BOOST_ANDROID_URL := https://github.com/moritz-wundke/Boost-for-Android/archive/c6012c576e30ff6000ddab0988d59bad849200ce.zip
BOOST_ANDROID_NAME := Boost-for-Android-c6012c576e30ff6000ddab0988d59bad849200ce
endif

BOOST_URL := https://kumisystems.dl.sourceforge.net/project/boost/boost/$(BOOST_VERSION)/$(BOOST_NAME).tar.bz2

PKGS += boost
ifeq ($(call need_pkg,'boost'),)
PKGS_FOUND += boost
endif

$(TARBALLS)/$(BOOST_NAME).tar.bz2:
	$(call download,$(BOOST_URL))

ifdef HAVE_ANDROID
$(TARBALLS)/$(BOOST_ANDROID_NAME).zip:
	$(call download,$(BOOST_ANDROID_URL))
endif

.sum-boost: $(BOOST_NAME).tar.bz2

ifdef HAVE_ANDROID
.sum-boost-android: $(BOOST_ANDROID_NAME).zip
endif


ifdef HAVE_ANDROID
boost: $(BOOST_ANDROID_NAME).zip
	$(UNPACK)
else
boost: $(BOOST_NAME).tar.bz2
	$(UNPACK)
endif

ifdef HAVE_ANDROID
.boost: boost toolchain.cmake .sum-boost
	cd $(BOOST_ANDROID_NAME) && cp -fr ../../tarballs/$(BOOST_NAME).tar.bz2 ./ && LIBRARIES=$(LIBRARIES) ./build-android.sh --layout=system --boost=$(BOOST_VERSION) --toolchain=llvm --arch=$(ANDROID_ABI) $(ANDROID_NDK) && cp -afr build/out/$(ANDROID_ABI)/* $(PREFIX)
	touch $@
else
.boost: boost toolchain.cmake .sum-boost
	cd $(BOOST_NAME) && ./bootstrap.sh --with-libraries=$(LIBRARIES) && ./b2 install -j8 --prefix=$(PREFIX) variant=release && ./b2 install -j8 --prefix=$(PREFIX) variant=release link=static
	touch $@
endif