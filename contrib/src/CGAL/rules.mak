CGAL_NAME := CGAL
CGAL_VERSION := 5.2.3
CGAL_URL := https://github.com/$(CGAL_NAME)/cgal/releases/download/v$(CGAL_VERSION)/$(CGAL_NAME)-$(CGAL_VERSION)-library.tar.xz

PKGS += $(CGAL_NAME)
ifeq ($(call need_pkg,'$(CGAL_NAME)'),)
PKGS_FOUND += $(CGAL_NAME)
endif

DEPS_CGAL += gmp
DEPS_CGAL += mpfr
DEPS_CGAL += boost
DEPS_CGAL += zlib

CGAL_CMAKECONF := -DCGAL_HEADER_ONLY=ON

$(TARBALLS)/$(CGAL_NAME)-$(CGAL_VERSION).tar.xz:
	$(call download,$(CGAL_URL))

.sum-$(CGAL_NAME): $(CGAL_NAME)-$(CGAL_VERSION).tar.xz

$(CGAL_NAME): $(CGAL_NAME)-$(CGAL_VERSION).tar.xz
	$(UNPACK)
	mv $(CGAL_NAME)-$(CGAL_VERSION) $(CGAL_NAME)

.$(CGAL_NAME): $(CGAL_NAME) toolchain.cmake .sum-$(CGAL_NAME)
	mkdir -p $</build
	cd $</build && $(CMAKE) $(CMAKECONF) $(CGAL_CMAKECONF) ..
	cd $</build && $(MAKE) install
	touch $@