# OPENDHT
OPENDHT_VERSION := 2.5.5
OPENDHT_URL := https://github.com/savoirfairelinux/opendht/archive/v$(OPENDHT_VERSION).tar.gz

PKGS += opendht
ifeq ($(call need_pkg,'opendht >= 2.3.2'),)
PKGS_FOUND += opendht
endif

# Avoid building distro-provided dependencies in case opendht was built manually
DEPS_opendht += msgpack-cxx argon2 libressl restinio jsoncpp nettle gmp gnutls fmt

ifdef HAVE_ANDROID
OPENDHT_CMAKECONF := -DBUILD_TESTING=OFF \
					 -DOPENDHT_TESTS=OFF \
                     -DOPENDHT_TESTS_NETWORK=OFF \
					 -DOPENDHT_TOOLS=OFF \
					 -DOPENDHT_SYSTEMD=OFF \
					 -DOPENDHT_PYTHON=OFF \
					 -DOPENDHT_INDEX=OFF \
					 -DOPENDHT_PROXY_SERVER=ON \
					 -DOPENDHT_PROXY_CLIENT=ON \
					 -DOPENDHT_PUSH_NOTIFICATIONS=ON \
					 -DOPENDHT_DOCUMENTATION=OFF \
					 -DOPENDHT_STATIC=ON
else
OPENDHT_CMAKECONF := -DBUILD_TESTING=OFF \
					 -DOPENDHT_TESTS=OFF \
                     -DOPENDHT_TESTS_NETWORK=OFF \
					 -DOPENDHT_TOOLS=OFF \
					 -DOPENDHT_SYSTEMD=OFF \
					 -DOPENDHT_PYTHON=OFF \
					 -DOPENDHT_INDEX=OFF \
					 -DOPENDHT_PROXY_SERVER=ON \
					 -DOPENDHT_PROXY_CLIENT=ON \
					 -DOPENDHT_PUSH_NOTIFICATIONS=ON \
					 -DOPENDHT_DOCUMENTATION=OFF \
					 -DOPENDHT_STATIC=ON
endif


# fmt 5.3.0 fix: https://github.com/fmtlib/fmt/issues/1267
#OPENDHT_CONF = FMT_USE_USER_DEFINED_LITERALS=0

$(TARBALLS)/opendht-$(OPENDHT_VERSION).tar.gz:
	$(call download,$(OPENDHT_URL))

.sum-opendht: opendht-$(OPENDHT_VERSION).tar.gz

opendht: opendht-$(OPENDHT_VERSION).tar.gz
	$(UNPACK)
	$(UPDATE_AUTOCONFIG) && cd $(UNPACK_DIR)
	$(MOVE)

.opendht: opendht .sum-opendht
	cd $< && mkdir -p build
	cd $< && cd build && $(HOSTVARS) $(OPENDHT_CONF) $(CMAKE) $(CMAKECONF) $(OPENDHT_CMAKECONF) ..
	cd $< && cd build && $(MAKE) install
	touch $@