MPFR_NAME := mpfr
MPFR_VERSION := 4.2.1
MPFR_URL := https://www.mpfr.org/mpfr-current/mpfr-$(MPFR_VERSION).tar.xz

PKGS += $(MPFR_NAME)
ifeq ($(call need_pkg,'$(MPFR_NAME)'),)
PKGS_FOUND += $(MPFR_NAME)
endif

DEPS_MPFR += gmp

$(TARBALLS)/$(MPFR_NAME)-$(MPFR_VERSION).tar.xz:
	$(call download,$(MPFR_URL))

.sum-$(MPFR_NAME): $(MPFR_NAME)-$(MPFR_VERSION).tar.xz

$(MPFR_NAME): $(MPFR_NAME)-$(MPFR_VERSION).tar.xz .sum-$(MPFR_NAME)
	$(UNPACK)
	$(MOVE)

.$(MPFR_NAME): $(MPFR_NAME)
	cd $< && $(HOSTVARS) ./configure $(HOSTCONF)
	cd $< && $(MAKE) -j$(nproc) install
	touch $@
