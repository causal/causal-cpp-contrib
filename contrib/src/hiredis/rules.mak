HIREDIS_NAME := hiredis
HIREDIS_VERSION := 1.0.2
HIREDIS_URL := https://github.com/redis/$(HIREDIS_NAME)/archive/refs/tags/v$(HIREDIS_VERSION).tar.gz

PKGS += $(HIREDIS_NAME)
ifeq ($(call need_pkg,'$(HIREDIS_NAME)'),)
PKGS_FOUND += $(HIREDIS_NAME)
endif

HIREDIS_CMAKECONF := -DJSONCPP_WITH_TESTS=OFF

$(TARBALLS)/$(HIREDIS_NAME)-$(HIREDIS_VERSION).tar.gz:
	$(call download,$(HIREDIS_URL))

.sum-$(HIREDIS_NAME): $(HIREDIS_NAME)-$(HIREDIS_VERSION).tar.gz

$(HIREDIS_NAME): $(HIREDIS_NAME)-$(HIREDIS_VERSION).tar.gz
	$(UNPACK)
	mv $(UNPACK_DIR) $(HIREDIS_NAME)

.$(HIREDIS_NAME): $(HIREDIS_NAME) toolchain.cmake .sum-$(HIREDIS_NAME)
	mkdir -p $</build
	cd $</build && $(CMAKE) $(CMAKECONF) $(HIREDIS_CMAKECONF) ..
	cd $</build && $(MAKE) install
	touch $@