QT_NAME := qt

QT_MAJOR := 6.7
QT_VERSION := 6.7.2

QT_URL := https://download.qt.io/official_releases/$(QT_NAME)/$(QT_MAJOR)/$(QT_VERSION)/single/$(QT_NAME)-everywhere-src-$(QT_VERSION).tar.xz

PKGS += $(QT_NAME)
ifeq ($(call need_pkg,'$(QT_NAME)'),)
PKGS_FOUND += $(QT_NAME)
endif

$(TARBALLS)/$(QT_NAME)-everywhere-src-$(QT_VERSION).tar.xz:
	$(call download,$(QT_URL))

.sum-$(QT_NAME): $(QT_NAME)-everywhere-src-$(QT_VERSION).tar.xz

$(QT_NAME): $(QT_NAME)-everywhere-src-$(QT_VERSION).tar.xz
	$(UNPACK)
	mv qt-everywhere-src-$(QT_VERSION) $(QT_NAME)

ifdef HAVE_ANDROID
.$(QT_NAME): $(QT_NAME) toolchain.cmake .sum-$(QT_NAME)
	cd $< && JAVA_HOME=/usr/lib/jvm/default-java \
	         PATH=/usr/lib/jvm/default-java/bin:$(PATH) \
			 CMAKE_PREFIX_PATH=$(PREFIX) \
			 ./configure -platform android-clang \
			             -qt-host-path $(PREFIX)/../x86_64-linux-gnu \
                         -prefix $(PREFIX) \
						 -headerdir $(PREFIX)/include/qt6 \
                         -disable-rpath \
                         -nomake tests \
                         -nomake examples \
						 -release \
                         -android-ndk $(ANDROID_NDK) \
                         -android-sdk $(ANDROID_SDK) \
                         -android-abis $(ANDROID_ABI) \
                         -opensource \
                         -confirm-license \
			 && $(CMAKE) --build . && $(CMAKE) --install .
	touch $@
else
.$(QT_NAME): $(QT_NAME) toolchain.cmake .sum-$(QT_NAME)
	cd $< && JAVA_HOME=/usr/lib/jvm/default-java \
	         PATH=/usr/lib/jvm/default-java/bin:$(PATH) \
			 CMAKE_PREFIX_PATH=$(PREFIX) \
			 ./configure -prefix $(PREFIX) \
						 -headerdir $(PREFIX)/include/qt6 \
                         -nomake tests \
                         -nomake examples \
						 -release \
                         -opensource \
                         -confirm-license \
			 && $(CMAKE) --build . && $(CMAKE) --install .
	touch $@
endif