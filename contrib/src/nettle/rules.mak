# Nettle

NETTLE_VERSION := nettle_3.8_release_20220602
#NETTLE_URL := https://git.lysator.liu.se/nettle/nettle/-/archive/$(NETTLE_VERSION)/nettle-$(NETTLE_VERSION).tar.gz
NETTLE_URL := https://github.com/gnutls/nettle/archive/refs/tags/$(NETTLE_VERSION).zip
PKGS += nettle

ifeq ($(call need_pkg,"nettle >= 3.6"),)
PKGS_FOUND += nettle
endif
DEPS_nettle = gmp

$(TARBALLS)/$(NETTLE_VERSION).zip:
	$(call download,$(NETTLE_URL))

.sum-nettle: $(NETTLE_VERSION).zip

nettle: $(NETTLE_VERSION).zip .sum-nettle
	$(UNPACK)
# wrong UNPACK_DIR assumed by main.mak
	mv nettle-$(NETTLE_VERSION) $(UNPACK_DIR)
	$(UPDATE_AUTOCONFIG)
	$(MOVE)

.nettle: nettle
ifdef HAVE_IOS
	cd $< && sed -i.orig s/-ggdb3//g configure.ac
endif
	cd $< && autoreconf
	cd $< && $(HOSTVARS) ./configure --disable-documentation $(HOSTCONF)
	cd $< && $(MAKE)
	cd $< && $(MAKE) install
	touch $@
