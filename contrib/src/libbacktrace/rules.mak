ifndef HAVE_ANDROID
BACKTRACE_NAME := libbacktrace
BACKTRACE_VERSION := 8602fda64e78f1f46563220f2ee9f7e70819c51d
BACKTRACE_URL := https://github.com/ianlancetaylor/$(BACKTRACE_NAME)/archive/$(BACKTRACE_VERSION).zip

PKGS += $(BACKTRACE_NAME)
ifeq ($(call need_pkg,'$(BACKTRACE_NAME)'),)
PKGS_FOUND += $(BACKTRACE_NAME)
endif

$(TARBALLS)/$(BACKTRACE_NAME)-$(BACKTRACE_VERSION).zip:
	$(call download,$(BACKTRACE_URL))

.sum-$(BACKTRACE_NAME): $(BACKTRACE_NAME)-$(BACKTRACE_VERSION).zip

$(BACKTRACE_NAME): $(BACKTRACE_NAME)-$(BACKTRACE_VERSION).zip
	$(UNPACK)
	mv $(UNPACK_DIR) $(BACKTRACE_NAME)

.$(BACKTRACE_NAME): $(BACKTRACE_NAME) toolchain.cmake .sum-$(BACKTRACE_NAME)
	cd $< && ./configure $(HOSTCONF) && $(MAKE) -j$(nproc) install
	touch $@
endif
