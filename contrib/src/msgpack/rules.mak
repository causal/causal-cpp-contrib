# MSGPACK
MSGPACK_VERSION := c-4.0.0
MSGPACK_URL := https://github.com/msgpack/msgpack-c/archive/$(MSGPACK_VERSION).tar.gz

PKGS += msgpack
ifeq ($(call need_pkg,"msgpack >= 3.1.0"),)
PKGS_FOUND += msgpack
endif

MSGPACK_CMAKECONF := -DMSGPACK_BUILD_EXAMPLES=OFF

$(TARBALLS)/msgpack-c-$(MSGPACK_VERSION).tar.gz:
	$(call download,$(MSGPACK_URL))

.sum-msgpack: msgpack-c-$(MSGPACK_VERSION).tar.gz

msgpack: msgpack-c-$(MSGPACK_VERSION).tar.gz .sum-msgpack
	$(UNPACK)
	$(MOVE)

.msgpack: msgpack toolchain.cmake
	mkdir -p $</build
	cd $</build && $(HOSTVARS) $(CMAKE) $(CMAKECONF) $(MSGPACK_CMAKECONF) ..
	cd $</build && $(MAKE) install
	touch $@
