#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

case "$1" in
    android)        mkdir -p tool || exit $?
                    cd tool || exit $?
                    rm -fr android || exit $?
                    mkdir android || exit $?
                    cd android || exit $?

                    echo "*** installing commandline tools"
                    wget -nv https://dl.google.com/android/repository/commandlinetools-linux-7583922_latest.zip || exit $?
                    unzip -q commandlinetools-linux-7583922_latest.zip || exit $?
                    rm -fr commandlinetools-linux-7583922_latest.zip
                    mv cmdline-tools bootstrap
                    yes | bootstrap/bin/sdkmanager --sdk_root=./ --licenses || exit $?
                    bootstrap/bin/sdkmanager --sdk_root=./ --install "cmdline-tools;latest" || exit $?
                    rm -fr bootstrap

                    echo "*** installing ndk"
                    cmdline-tools/latest/bin/sdkmanager --sdk_root=./ --install "ndk;25.2.9519653" || exit $?

                    echo "*** installing platform tools"
                    cmdline-tools/latest/bin/sdkmanager --sdk_root=./ --install "platform-tools" || exit $?
                    cmdline-tools/latest/bin/sdkmanager --sdk_root=./ --install "platforms;android-33" || exit $?

                    echo "*** installing build tools"
                    cmdline-tools/latest/bin/sdkmanager --sdk_root=./ --install "build-tools;33.0.2" || exit $?

                    keytool -genkey -noprompt \
                            -keystore debug.keystore \
                            -alias debug \
                            -keyalg RSA -keysize 2048 \
                            -dname "CN=debug.deb, OU=ID, O=debug, L=debug, S=debug, C=DB" \
                            -validity 10000 \
                            -storepass debug123

                    #echo "*** installing system images"
                    #cmdline-tools/latest/bin/sdkmanager --sdk_root=./ --install "emulator" || exit $?
                    #cmdline-tools/latest/bin/sdkmanager --sdk_root=./ --install "system-images;android-33;default;arm64-v8a" || exit $?
                    #cmdline-tools/latest/bin/sdkmanager --sdk_root=./ --install "system-images;android-33;google_apis;arm64-v8a" || exit $?
                    ;;
    cmake)          mkdir -p tool || exit $?
                    cd tool || exit $?
                    rm -fr cmake* || exit $?
                    curl -L -s -o cmake-3.27.4.tar.gz https://github.com/Kitware/CMake/releases/download/v3.27.4/cmake-3.27.4-linux-x86_64.tar.gz || exit $?
                    tar xzf cmake-3.27.4.tar.gz || exit $?
                    ln -s cmake-3.27.4-linux-x86_64 cmake
                    ;;
    *)              ;;
esac
